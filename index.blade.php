@extends('adminlte.master1')

@section('content')
			<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Created new post</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/post" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="title"><Title></label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title">
                  </div>
                  <div class="form-group">
                    <label for="Body">Password</label>
                    <input type="Text" class="form-control" id="body" name="body" placeholder="Password">
                  </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>			
@endsection
